#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <dlfcn.h>

#include "cJSON.h"
#include "xtime.h"

char* (*dev_func)(char *param);
int (*checkpoint)(), (*restore)(), (*dump_stats)();
void* myso;
FILE* out_fp;

void setup(){
	myso = dlopen("./main.so", RTLD_NOW);
	*(void **)(&dev_func) = dlsym(myso, "func");
	myso = dlopen("/bin/build/groundhog-tracee-lib.so", RTLD_NOW);
	*(void **)(&checkpoint) = dlsym(myso, "checkpoint_me");
	*(void **)(&restore) = dlsym(myso, "restore_me");
	*(void **)(&dump_stats) = dlsym(myso, "dump_stats_me");
	out_fp = fdopen(3, "wb");
}

void should_ack(){
	if(getenv("__OW_WAIT_FOR_ACK")) {
		char *OW_ACK = "{\"ok\":true}\n";
		int r = fwrite(OW_ACK, sizeof(char), strlen(OW_ACK), out_fp);
		fflush(out_fp);
	}
}

int request_loop(){
	char* payload = "{}";
	size_t buffersiz = BUFSIZ;
	char* req_line = malloc(BUFSIZ);
	uint64_t start_time;
	uint64_t duration;
	char* func_ret;
	char* ret;
	char* end_marker = "\n";
	int i = 0;
	while(1){
		if (i == 1){
			//printf("Will checkpoint now\n");
			fflush(stdout);
			fflush(stderr);
			checkpoint();
			//printf("Checkpointed/restored successfully\n");
		}
		if (i > 1){
			//printf("Will restore now\n");
			restore();
		}
		while (getline(&req_line, &buffersiz, stdin) > 0){
			//printf("Got an argument %s \n", req_line);
			cJSON *req_json = cJSON_Parse(req_line);
			cJSON *func_args = cJSON_GetObjectItemCaseSensitive(req_json, "value");
			cJSON *is_dump_stats = cJSON_GetObjectItemCaseSensitive(func_args, "groundhog-dump-stats");

			if (is_dump_stats)
				dump_stats();

			payload = cJSON_PrintUnformatted(func_args);

			start_time = NOW();
			func_ret = dev_func(payload);
			duration = NOW() - start_time;

			cJSON *ret_json = cJSON_Parse(func_ret);
			cJSON_AddItemToObject(ret_json, "TrueTime", cJSON_CreateNumber(duration));
			ret = cJSON_PrintUnformatted(ret_json);
			//printf("returned.... will write response: %s, %s, %p\n", func_ret, ret, ret);
			fwrite(ret, sizeof(char), strlen(ret), out_fp);
			fwrite(end_marker, sizeof(char), strlen(end_marker), out_fp);
			fflush(out_fp);

			fflush(stdout);
			fflush(stderr);
			cJSON_Delete(req_json);
			cJSON_Delete(ret_json);
			free(ret);
			break;
		}
		i++;
	}
	return 0;
}

int main(int argc, char *argv[]) {
	setup();
	should_ack();
	request_loop();
	dlclose(myso);
}
